/**
 * 
 */
package com.mykart.product.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.mykart.product.dto.OrderInfo;
import com.mykart.product.request.CartRequest;
import com.mykart.product.response.CartResponse;
import com.mykart.product.service.CartService;
import com.mykart.product.service.OrderFeignService;
import com.mykart.product.util.CartUtil;

import feign.FeignException;

/**
 * @author rupesh.gurjar
 *
 */
@ExtendWith(MockitoExtension.class)

public class CartControllerTest {
	@Mock
	CartService cartService;

	@Mock
	OrderFeignService orderFeignService;

	@Mock
	CartUtil cartUtil;

	@InjectMocks
	CartController controller;

	/**
	 * @throws java.lang.Exception
	 */

	@BeforeEach
	void setUp() throws Exception {

	}

	/**
	 * Test method for {@link com.mykart.product.controller.CartController#test()}.
	 */

	@Test
	void testTest() {
		String myResponse = "Product service is up & running";
		String methodResposne = controller.test();
		assertEquals(myResponse, methodResposne);
	}

	/**
	 * Test method for
	 * {@link com.mykart.product.controller.CartController#allItems(long)}.
	 */

	@Test
	void testAllItems() {
		List<CartResponse> response = new ArrayList<CartResponse>();
		when(cartService.findAllItems(1)).thenReturn(response);
		List<CartResponse> output = (List<CartResponse>) controller.allItems(1).getBody();
		assertEquals(response, output);
	}

	/**
	 * Test method for
	 * {@link com.mykart.product.controller.CartController#addToCart(com.mykart.product.request.CartRequest)}.
	 */

	@Test
	void testAddToCart() {
		CartRequest request = new CartRequest();
		CartResponse response   = new CartResponse();
		when(cartService.addToCart(request)).thenReturn(response);
		CartResponse output   = (CartResponse) controller.addToCart(request).getBody();
		assertEquals(response, output);
	}

	/**
	 * Test method for
	 * {@link com.mykart.product.controller.CartController#placeOrder(com.mykart.product.dto.OrderInfo)}.
	 */

	@Test
	void testPlaceOrder() {
		OrderInfo orderInfo = new OrderInfo();
		ResponseEntity<Object> response = new ResponseEntity<Object>("OrderPlaced",HttpStatus.OK);
		when(orderFeignService.createOrder(orderInfo)).thenReturn(response);
//		when(cartService.removeFromCart(1)).thenReturn(object);
		ResponseEntity<Object> output   =  controller.placeOrder(orderInfo);
		assertEquals(response, output);
	}

	/**
	 * Test method for
	 * {@link com.mykart.product.controller.CartController#removeFromCart(long)}.
	 */

	@Test
	void testRemoveFromCart() {
		ResponseEntity<Object> response = new ResponseEntity<Object>("Item removed from the cart",HttpStatus.OK);
		doNothing().when(cartService).removeFromCart(1);
		ResponseEntity<Object> output   =  controller.removeFromCart(1);
		assertEquals(response, output);
	}

	@Test
	void testFallbackOrder() {
		FeignException input = new FeignException(200,"OK") {
		};
		ResponseEntity<Object> response = new ResponseEntity<Object>("Item removed from the cart",HttpStatus.OK);
		ResponseEntity<Object> output   =  controller.removeFromCart(1);
		assertEquals(response, output);
	}

}
