package com.mykart.product.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import com.mykart.product.entity.Product;
import com.mykart.product.exception.ProductNotFoundException;
import com.mykart.product.mapper.ProductMapper;
import com.mykart.product.request.ProductRequest;
import com.mykart.product.response.ProductResponse;
import com.mykart.product.service.ProductService;

@ExtendWith(MockitoExtension.class)
public class ProductControllerTest {

	@Mock
	ProductService productService;

	@Mock
	Environment env;

	@Mock
	ProductMapper productMapper;
	
	@InjectMocks
	ProductController controller;

	List<ProductResponse> list;

	ResponseEntity<Object> response;

	ProductResponse productResposne;

	ProductRequest productRequest;

	Pageable pageable;

	Page<Product> page;

	@BeforeEach
	void setUp() throws Exception {
		pageable = PageRequest.of(0, 3);
		productRequest = new ProductRequest("sdfds", "sdfds", 212, "sdf", "dfd", 2);
		productResposne = new ProductResponse("sdfds", "sdfds", 212, "sdf", "dfd", 2);

		list = new ArrayList<ProductResponse>();
		list.add(new ProductResponse("sdfds", "sdfds", 212, "sdf", "dfd", 2));
		list.add(new ProductResponse("sdfds", "sdfds", 212, "sdf", "dfd", 2));
	}

	@Test
	@DisplayName("allProducts(): Positive")
	void testAllProducts() {
		// given
		when(productService.findAllProducts()).thenReturn(list);

		// when
		List<ProductResponse> result = (List<ProductResponse>) controller.allProducts().getBody();

		// then
		assertEquals(2, result.size());

	}

	@Test
	@DisplayName("allProducts(): Negative")
	void testAllProductsNoDataFound() {
		// given
		when(productService.findAllProducts()).thenThrow(ProductNotFoundException.class);
		// when && Verify
		assertThrows(ProductNotFoundException.class, () -> productService.findAllProducts());

	}

	@Test
	@DisplayName("productByCode(): Positive")
	void testProductByCode() {
		// given
		when(productService.findProductByCode("dummy")).thenReturn(productResposne);

		// when
		ProductResponse result = (ProductResponse) controller.productByCode("dummy").getBody();

		// then
		assertEquals(productResposne, result);
	}

	@Test
	@DisplayName("addProduct(): Positive")
	void testAddProduct() {
		// given
		when(productService.save(productRequest)).thenReturn(productResposne);

		// when
		ProductResponse result = (ProductResponse) controller.addProduct(productRequest).getBody();

		// then
		assertEquals(productResposne, result);
	}

	@Test
	@DisplayName("allProductsByName(): Positive")
	void testAllProductsByName() {
		// given
		when(productService.findProdctByName("dummy")).thenReturn(list);

		// when
		List<ProductResponse> result = (List<ProductResponse>) controller.allProductsByName("dummy").getBody();

		// then
		assertEquals(2, result.size());
	}

	@Test
	void testAllProductsByCategory() {
		// given
		when(productService.findProductByCategory("dummy")).thenReturn(list);

		// when
		List<ProductResponse> result = (List<ProductResponse>) controller.allProductsByCategory("dummy").getBody();

		// then
		assertEquals(2, result.size());
	}

	 @Test
	void testFindAll() {
		Map<String, Object> response = new HashMap<>();
		// given
		when(productService.findAllUsingPagination(pageable)).thenReturn(page);
		when(productMapper.getResponse(page)).thenReturn(response);

		// when
		Map<String, Object> result = (Map<String, Object>) controller.findAll(0, 3).getBody();
		// then
		assertEquals(response, result);
	}

	@Test
	void testReduceProduct() {
		// given
		when(productService.updateProduct(productRequest)).thenReturn(productResposne);

		// when
		ProductResponse result = (ProductResponse) controller.reduceProduct(productRequest).getBody();
		// then
		assertEquals(productResposne, result);
	}

	@Test
	void testTest() {
		String port = "Product service is up & running at the 8080";
		when(env.getProperty("server.port")).thenReturn("8080");
		String myPort = controller.test();
		assertEquals(port, myPort);
	}

}
