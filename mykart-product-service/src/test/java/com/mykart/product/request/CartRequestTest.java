package com.mykart.product.request;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
@ExtendWith(MockitoExtension.class)
public class CartRequestTest {

	@Mock
	CartRequest request;

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	public void testGetCustomerId() {
		when(request.getCustomerId()).thenReturn(1l);
		long l = request.getCustomerId();
		assertEquals(1l, l);
	}

	@Test
	public void testGetProductId() {
		when(request.getProductId()).thenReturn(1l);
		long l = request.getProductId();
		assertEquals(1l, l);
	}

	@Test
	public void testGetProductName() {
		String response = "Product";
		when(request.getProductName()).thenReturn(response.toString());
		String output = request.getProductName();
		assertEquals(response,output);
	}

	@Test
	public void testGetDescription() {
		String response = "Description";
		when(request.getDescription()).thenReturn(response.toString());
		String output = request.getDescription();
		assertEquals(response,output);
  }

	@Test
	public void testGetPrice() {
		BigDecimal response = new BigDecimal(0);
		when(request.getPrice()).thenReturn(response);
		BigDecimal output = request.getPrice();
		assertEquals(response, output);
	}

	@Test
	public void testGetQuantity() {
		when(request.getQuantity()).thenReturn(1);
		int l = request.getQuantity();
		assertEquals(1, l);
	}

	@Test
	public void testSetCustomerId() {
		doNothing().when(request).setCustomerId(10);
		request.setCustomerId(10);
		verify(request,times(1)).setCustomerId(10);
	}

	@Test
	void testSetProductId() {
		doNothing().when(request).setProductId(10);
		request.setProductId(10);
		verify(request,times(1)).setProductId(10);
	}

	@Test
	void testSetProductName() {
		doNothing().when(request).setProductName("Product");
		request.setProductName("Product");
		verify(request,times(1)).setProductName("Product");
	}

	@Test
	void testSetDescription() {
		doNothing().when(request).setDescription("Description");
		request.setDescription("Description");
		verify(request,times(1)).setDescription("Description");
	}

	@Test
	void testSetPrice() {
		BigDecimal response = new BigDecimal(10);
		doNothing().when(request).setPrice(response);
		request.setPrice(response);
		verify(request,times(1)).setPrice(response);
	}

	@Test
	void testSetQuantity() {
		doNothing().when(request).setQuantity(10);
		request.setQuantity(10);
		verify(request,times(1)).setQuantity(10);
	}

	@Test
	void testCartRequest() {
		doNothing().when(request).setProductId(10);
		request.setProductId(10);
		verify(request,times(1)).setProductId(10);
	}

	@Test
	void testCartRequestLongLongStringStringBigDecimalInt() {
		doNothing().when(request).setProductId(10);
		request.setProductId(10);
		verify(request,times(1)).setProductId(10);
	}
}
