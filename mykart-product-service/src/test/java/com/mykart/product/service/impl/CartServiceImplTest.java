package com.mykart.product.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.mykart.product.entity.Cart;
import com.mykart.product.mapper.CartMapper;
import com.mykart.product.repository.CartRepository;
import com.mykart.product.request.CartRequest;
import com.mykart.product.response.CartResponse;

@ExtendWith(MockitoExtension.class)
public class CartServiceImplTest {
	@Mock CartRepository cartRepository;

	@Mock CartMapper cartMapper;

	@Mock CartRequest request;
	
	@Mock CartResponse response;
	
	@Mock Cart cart;

	@InjectMocks CartServiceImpl service;

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testAddToCart() {
		when(cartRepository.findByCustomerIdAndCode(request.getCustomerId(), request.getProductId())).thenReturn(cart);
		when(cartMapper.mapCartRequest(request,cart)).thenReturn(cart);
		when(cartRepository.save(cart)).thenReturn(cart);
		when(cartMapper.mapCartResponse(cart)).thenReturn(response);
		CartResponse output = service.addToCart(request);
		assertEquals(response, output);
		
	}

	@Test
	void testRemoveFromCartLongLong() {
		doNothing().when(cartRepository).deleteByCustomerIdAndCode(1,1);
		service.removeFromCart(1,1);
		verify(cartRepository,times(1)).deleteByCustomerIdAndCode(1,1);
	}

	//@Test
	void testFindByOrderId() {
		fail("Not yet implemented");
	}

	@Test
	void testFindAllItems() {
		List<Cart> cartList = new ArrayList<>();
		List<CartResponse> cartResponseList = new ArrayList<>();
		when(cartRepository.findAllByCustomerId(request.getCustomerId())).thenReturn(cartList);
		when(cartMapper.mapCartResponseList(cartList)).thenReturn(cartResponseList);
		List<CartResponse> response  = service.findAllItems(request.getCustomerId());
		assertEquals(cartResponseList, response);
	}

	@Test
	void testRemoveFromCartLong() {
		doNothing().when(cartRepository).deleteByCustomerId(1);
		service.removeFromCart(1);
		verify(cartRepository,times(1)).deleteByCustomerId(1);
}

	@Test
	void testFindByCustomerAndProductId() {
		when(cartRepository.findByCustomerIdAndCode(request.getCustomerId(), request.getProductId())).thenReturn(cart);
		when(cartMapper.mapCartResponse(cart)).thenReturn(response);
		CartResponse output = service.findByCustomerAndProductId(request.getCustomerId(), request.getProductId());
		assertEquals(response, output);
	}

}
