package com.mykart.product.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.mykart.product.entity.Product;
import com.mykart.product.exception.ProductNotFoundException;
import com.mykart.product.mapper.ProductMapper;
import com.mykart.product.repository.ProductRepository;
import com.mykart.product.response.ProductResponse;
import com.mykart.product.util.ProductUtil;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class ProductServiceImplTest {
	
	@Mock
	private ProductRepository productRepository;

	@Mock
	private ProductMapper productMapper;

	@Mock
	private ProductUtil productUtil;
	
	@InjectMocks
	ProductServiceImpl serviceImpl;
	
	List<Product> list;
	
	List<ProductResponse> productResponseList;
	
	Optional<Product>  product;
	@BeforeAll
	void setUp() throws Exception {
		list = new ArrayList<Product>();
		list.add(new Product(1l,"1","name1","category1","type1","description1",1,1));
		list.add(new Product(2l,"2","name2","category2","type2","description2",2,2));
		list.add(new Product(3l,"3","name3","category3","type3","description3",3,3));
		list.add(new Product(4l,"4","name4","category4","type4","description4",4,4));
		
		productResponseList = new ArrayList<ProductResponse>();
		productResponseList.add(new ProductResponse("name1","description1",1,"category1","type1",1));
		productResponseList.add(new ProductResponse("name2","description2",2,"category2","type2",2));
		productResponseList.add(new ProductResponse("name3","description3",3,"category3","type3",3));
		productResponseList.add(new ProductResponse("name4","description4",4,"category4","type4",4));
		
		product =  Optional.of(new Product(2l,"2","name2","category2","type2","description2",2,2));
	}
	@Test
	@DisplayName("findAllProducts : Positive")
	void testfindAllProducts() {
		when(productRepository.findAll()).thenReturn(list);
		when(productMapper.getAllProducts(list)).thenReturn(productResponseList);
		List<ProductResponse> result = serviceImpl.findAllProducts();
		assertEquals(4, result.size());
	}

	
	@Test
	@DisplayName("findProductByCode : Positive")
	void testFindProductByCode() {
		when(productRepository.findByCode("1")).thenReturn(product);
		Optional<Product> productResult  = productRepository.findByCode("1");
		verify(productRepository).findByCode("1");
		assertEquals(product, productResult);
	}
	@Test
	@DisplayName("findProductByCode : Negative")
	void testFindProductByCodeEmpty() {
		when(productRepository.findByCode("2")).thenReturn(product);
		Optional<Product> result = productRepository.findByCode("2").empty();
		verify(productRepository).findByCode("2");
		assertEquals(Optional.empty(), result.empty());
	}
	@Test
	@DisplayName("findAll : Positive")
	void testFindAll() {
		when(productRepository.findAll()).thenReturn(list);
		List<Product> result = productRepository.findAll();
		verify(productRepository).findAll();
		assertEquals(4, result.size());
	}
	
	@Test
	@DisplayName("findAll(): Negative")
	void testFindAllNoDataFound() {
		//given
		when(productRepository.findAll()).thenThrow(ProductNotFoundException.class);
		//when && Verify
		assertThrows(ProductNotFoundException.class, ()->productRepository.findAll());
		
	}

}
