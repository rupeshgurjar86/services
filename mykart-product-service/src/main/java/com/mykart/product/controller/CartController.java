package com.mykart.product.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mykart.product.dto.OrderInfo;
import com.mykart.product.request.CartRequest;
import com.mykart.product.response.CartResponse;
import com.mykart.product.service.CartService;
import com.mykart.product.service.OrderFeignService;
import com.mykart.product.util.CartUtil;

import feign.FeignException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/cart")

public class CartController {
	private static final Logger log = LoggerFactory.getLogger(CartController.class);
	@Autowired
	private CartService cartService;

	@Autowired
	private OrderFeignService orderFeignService;

	@Autowired
	private CartUtil cartUtil;

	@ApiIgnore
	@GetMapping("/api")
	public String test() {
		return "Product service is up & running";
	}

	@ApiIgnore
	@GetMapping("/{customerId}")
	public ResponseEntity<Object> allItems(@PathVariable("customerId") long customerId) {
		log.debug("CartController all items called");
		List<CartResponse> response = cartService.findAllItems(customerId);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<Object> addToCart(@Valid @RequestBody CartRequest request) {
		log.debug("CartController addToCart called");
		CartResponse response = cartService.addToCart(request);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@PostMapping("/orders")
	@CircuitBreaker(name = "orderservice", fallbackMethod = "fallbackOrder")
	public ResponseEntity<Object> placeOrder(@RequestBody OrderInfo orderInfo) {
		log.debug("CartController placeOrder called");
		ResponseEntity<Object> object = orderFeignService.createOrder(orderInfo);
		cartService.removeFromCart(orderInfo.getCustomerId());
		return new ResponseEntity<Object>(object.getBody(), HttpStatus.OK);
	}

	@DeleteMapping("/orders/{customerId}")
	public ResponseEntity<Object> removeFromCart(@PathVariable("customerId") long customerId) {
		log.debug("CartController deleteByCustomerId called");
		cartService.removeFromCart(customerId);
		return new ResponseEntity<Object>("Item removed from the cart", HttpStatus.OK);
	}

	private ResponseEntity<Object> fallbackOrder(FeignException ex) {
		return cartUtil.parseException(ex, "Order");
	}

}
