package com.mykart.product.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mykart.product.entity.Product;
import com.mykart.product.mapper.ProductMapper;
import com.mykart.product.request.ProductRequest;
import com.mykart.product.response.ProductResponse;
import com.mykart.product.service.ProductService;

import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/products")

public class ProductController {
	private static final Logger log = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductService productService;

	@Autowired
	private Environment env;

	@Autowired
	private ProductMapper productMapper;

	@GetMapping("")
	public ResponseEntity<Object> allProducts() {
		log.info("ProductController allProducts called");
		List<ProductResponse> response = productService.findAllProducts();
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@GetMapping("/{code}")
	public ResponseEntity<Object> productByCode(@PathVariable String code) {
		log.info("ProductController productByCode called");
		ProductResponse response = productService.findProductByCode(code);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<Object> addProduct(@Valid @RequestBody ProductRequest request) {
		log.info("ProductController addProduct called");
		ProductResponse response = productService.save(request);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@GetMapping("/name/{name}")
	public ResponseEntity<Object> allProductsByName(@PathVariable("name") String name) {
		log.info("ProductController allProductByName called");
		List<ProductResponse> response = productService.findProdctByName(name);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@GetMapping("/category/{category}")
	public ResponseEntity<Object> allProductsByCategory(@PathVariable("category") String category) {
		log.info("ProductController allProductByCategory called");
		List<ProductResponse> response = productService.findProductByCategory(category);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@GetMapping("/pagination")
	public ResponseEntity<Object> findAll(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "3") int size) {

		Pageable pageable = PageRequest.of(page, size);

		Page<Product> pages = productService.findAllUsingPagination(pageable);
		Map<String, Object> response = productMapper.getResponse(pages);

		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@ApiIgnore
	@PutMapping("")
	public ResponseEntity<Object> reduceProduct(@RequestBody ProductRequest request) {
		log.info("ProductController reduce product called");
		ProductResponse response = productService.updateProduct(request);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@ApiIgnore
	@GetMapping("/api")
	public String test() {
		String port = env.getProperty("server.port");
		return "Product service is up & running at the " + port;
	}

}
