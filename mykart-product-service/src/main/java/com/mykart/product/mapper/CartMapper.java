package com.mykart.product.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.mykart.product.entity.Cart;
import com.mykart.product.exception.ProductNotFoundException;
import com.mykart.product.request.CartRequest;
import com.mykart.product.response.CartResponse;

@Component
public class CartMapper {
	public CartResponse mapCartResponse(Cart cart) {
		CartResponse response = new CartResponse(cart.getCustomerId(), cart.getCode(), cart.getName(),
				cart.getDescription(), cart.getPrice(), cart.getQuantity());
		return response;
	}

	public Cart mapCartRequest(CartRequest request, Cart cart) {
		Cart newCart = new Cart();
		newCart.setName(request.getProductName());
		newCart.setCustomerId(request.getCustomerId());
		newCart.setCode(request.getProductId());
		newCart.setDescription(request.getDescription());
		newCart.setPrice(request.getPrice());
		if (null != cart && cart.getCustomerId() == request.getCustomerId()) {
			cart.setQuantity(request.getQuantity() + cart.getQuantity());
			return cart;
		} else {
			newCart.setQuantity(request.getQuantity());
			return newCart;
		}
	}

	public List<CartResponse> mapCartResponseList(List<Cart> cartList) {
		if (cartList.size() == 0)
			throw new ProductNotFoundException("Cart is empty");
		List<CartResponse> cartResponseList = new ArrayList<CartResponse>();

		for (Cart item : cartList) {
			CartResponse cartResponse = new CartResponse();
			cartResponse.setCustomerId(item.getCustomerId());
			cartResponse.setDescription(item.getDescription());
			cartResponse.setProductName(item.getName());
			cartResponse.setPrice(item.getPrice());
			cartResponseList.add(cartResponse);
		}

		return cartResponseList;
	}
}
