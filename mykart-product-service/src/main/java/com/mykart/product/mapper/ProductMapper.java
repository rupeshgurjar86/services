package com.mykart.product.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.mykart.product.entity.Product;
import com.mykart.product.exception.ProductNotFoundException;
import com.mykart.product.request.ProductRequest;
import com.mykart.product.response.ProductResponse;

@Component
public class ProductMapper {
	public Product mapProductRequest(ProductRequest request) {
		Product product = new Product();
		product.setName(request.getProductName());
		product.setDescription(request.getDescription());
		product.setPrice(request.getProductPrice());
		product.setType(request.getType());
		product.setCategory(request.getCategory());
		return product;

	}

	public ProductResponse mapProductResponse(Product product) {
		ProductResponse productResponse = new ProductResponse();
		productResponse.setProductName(product.getName());
		productResponse.setDescription(product.getDescription());
		productResponse.setProductPrice(product.getPrice());
		productResponse.setType(product.getType());
		productResponse.setCategory(product.getCategory());
		productResponse.setQuantity(product.getQauntity());
		return productResponse;
	}

	public Map<String, Object> getResponse(Page<Product> pages) {
		Map<String, Object> response = new HashMap<>();
		List<Product> products = pages.getContent();
		List<ProductResponse> productResponseList = getAllProducts(products);
		response.put("products", productResponseList);
		response.put("currentPage", pages.getNumber());
		response.put("totalItems", pages.getTotalElements());
		response.put("totalPages", pages.getTotalPages());
		return response;
	}

	public List<ProductResponse> getAllProducts(List<Product> products) {
		if (products.size() == 0 || products.isEmpty())
			throw new ProductNotFoundException("Product list is empty");
		List<ProductResponse> productResponseList = new ArrayList<>();
		for (Product product : products) {

			ProductResponse productInfo = new ProductResponse(product.getName(), product.getDescription(),
					product.getPrice(), product.getCategory(), product.getType(), product.getQauntity());
			productResponseList.add(productInfo);
		}
		return productResponseList;
	}
}
