package com.mykart.product.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name = "cart")
@ToString
public class Cart {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private long customerId;
	@Column(nullable = false, unique = true)
	private long code;
	@Column(nullable = false)
	private String name;
	private String description;
	private BigDecimal price;
	private int quantity;
	
	 public BigDecimal getPrice() {
	        return price.multiply(new BigDecimal(quantity));
	    }

}
