package com.mykart.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MykartProductServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MykartProductServiceApplication.class, args);
	}

}
