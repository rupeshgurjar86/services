package com.mykart.product.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProductRequest {
	@NotEmpty(message = "Product name can not be empty.")
	@NotNull(message = "Product name ID can not be null.")
	private String productName;

	@NotEmpty(message = "Description can not be empty.")
	@NotNull(message = "Description can not be null.")
	private String description;

	@NotEmpty(message = "Product price can not be empty.")
	@NotNull(message = "Product price ID can not be null.")
	private double productPrice;

	@NotEmpty(message = "Category can not be empty.")
	@NotNull(message = "Category can not be null.")
	private String category;

	@NotEmpty(message = "Type can not be empty.")
	@NotNull(message = "Type can not be null.")
	private String type;

	@NotEmpty(message = "Quantity can not be empty.")
	@NotNull(message = "Quantity can not be null.")
	private int quantity;

}
