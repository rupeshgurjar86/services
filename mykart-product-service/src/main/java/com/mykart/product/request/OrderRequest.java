package com.mykart.product.request;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.mykart.product.dto.OrderItemInfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderRequest {
	@NotEmpty(message = "Customer ID can not be empty.")
	@NotNull(message = "Customer ID can not be null.")
	private long customerId;

	@NotEmpty(message = "Email  can not be empty.")
	@NotNull(message = "Email can not be null.")
	@Email(message = "Emai ID is not valid ")
	private String customerEmail;

	@NotEmpty(message = "Address can not be empty.")
	@NotNull(message = "Address can not be null.")
	private String customerAddress;

	@NotEmpty(message = "Order list can not be empty.")
	@NotNull(message = "Order list can not be null.")
	private List<OrderItemInfo> items;

	@NotEmpty(message = "Order Date can not be empty.")
	@NotNull(message = "Order Date can not be null.")
	private LocalDateTime orderDate;
	
	@NotEmpty(message = "Total Price can not be empty.")
	@NotNull(message = "Total Price can not be null.")
	private double totalPrice;
}
