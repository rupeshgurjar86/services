package com.mykart.product.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CartRequest {
	@NotEmpty(message = "Customer ID can not be empty.")
	@NotNull(message = "Customer ID can not be null.")
	private long customerId;

	@NotEmpty(message = "Product ID can not be empty.")
	@NotNull(message = "Product ID can not be null.")
	private long productId;

	@NotEmpty(message = "Product Name can not be empty.")
	@NotNull(message = "Product Name can not be null.")
	private String productName;

	private String description;

	@NotEmpty(message = "Price can not be empty.")
	@NotNull(message = "price can not be null.")
	private BigDecimal price;

	@NotEmpty(message = "Quantity can not be empty.")
	@NotNull(message = "Quantity can not be null.")
	private int quantity;
}
