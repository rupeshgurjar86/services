package com.mykart.product.request;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderItemsRequest {
	private Long productId;
	private int quantity;
	private BigDecimal productPrice;
	public BigDecimal getPrice() {
		return productPrice.multiply(new BigDecimal(quantity));
	}
}
