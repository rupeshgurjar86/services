package com.mykart.product.dto;

import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderInfo{
	private long customerId;
	private String customerEmail;
	private String customerAddress;
	private List<OrderItemInfo> items;
	private LocalDateTime orderDate;
}
