package com.mykart.product.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderResponse {
	private long customerId;
	private String customerEmail;
	private String customerAddress;
	private LocalDateTime orderDate;
	private BigDecimal totalAmount;
	private String status;
}
