package com.mykart.product.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mykart.product.entity.Product;
import com.mykart.product.repository.ProductRepository;
@Component
public class ProductUtil {
	private static Long nextProductCode = 00000001l;
	
	@Autowired
	private ProductRepository productRepository;
	public String getProductCode() {
		Product product = null;
		if (nextProductCode == 00000001) {
			product = new Product();
			product = productRepository.findFirstByOrderByCodeDesc();
			if (null != product)
				nextProductCode = Long.valueOf(product.getCode());
		}
		String prodCode = String.valueOf(++nextProductCode);
		return prodCode;
	}

}
