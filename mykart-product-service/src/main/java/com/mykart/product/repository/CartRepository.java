package com.mykart.product.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mykart.product.entity.Cart;

public interface CartRepository extends CrudRepository<Cart, Long> {
	public void deleteByCustomerIdAndCode(long customerId, long code);

	public void deleteByCustomerId(long customerId);

	List<Cart> findAllByCustomerId(long customerId);

	Cart findByCustomerIdAndCode(long customerId, long code);

}
