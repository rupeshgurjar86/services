package com.mykart.product.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mykart.product.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findByCode(String code);
    Optional<List<Product>> findByNameContaining(String name);
    Product findFirstByOrderByCodeDesc();
	Product findByCategoryAndNameAndType(String category,String name,String type);
	List<Product> findByCategory(String category);
}
