package com.mykart.product.response;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartResponse {
	private long customerId;
	private long productId;
	private String productName;
	private String description;
	private BigDecimal price;
	private int quantity;
}
