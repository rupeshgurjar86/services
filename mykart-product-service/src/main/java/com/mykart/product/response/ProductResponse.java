package com.mykart.product.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProductResponse {
	private String productName;

	private String description;

	private double productPrice;

	private String category;

	private String type;

	private int quantity;

}
