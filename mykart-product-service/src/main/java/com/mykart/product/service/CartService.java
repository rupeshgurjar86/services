package com.mykart.product.service;

import java.util.List;

import com.mykart.product.request.CartRequest;
import com.mykart.product.response.CartResponse;
import com.mykart.product.response.ProductResponse;

public interface CartService {
	CartResponse addToCart(CartRequest request);

	void removeFromCart(long customerId, long productId);

	void removeFromCart(long customerId);

	ProductResponse findByOrderId(long productId);

	List<CartResponse> findAllItems(long customerId);

	CartResponse findByCustomerAndProductId(long customerId,long code);

}
