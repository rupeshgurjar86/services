package com.mykart.product.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mykart.product.entity.Product;
import com.mykart.product.exception.ProductNotFoundException;
import com.mykart.product.mapper.ProductMapper;
import com.mykart.product.repository.ProductRepository;
import com.mykart.product.request.ProductRequest;
import com.mykart.product.response.ProductResponse;
import com.mykart.product.service.ProductService;
import com.mykart.product.util.ProductUtil;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductMapper productMapper;

	@Autowired
	private ProductUtil productUtil;

	@Override
	public List<ProductResponse> findAllProducts() {
		List<Product> products = productRepository.findAll();
		List<ProductResponse> productResponseList = productMapper.getAllProducts(products);
		return productResponseList;
	}

	public ProductResponse findProductByCode(String code) {
		Product product = productRepository.findByCode(code)
				.orElseThrow(() -> new ProductNotFoundException("Product with code [" + code + "] doesn't exist"));
	System.out.println(" FindProductByCode :"+product);
		return productMapper.mapProductResponse(product);
	}

	@Override
	public ProductResponse save(ProductRequest request) {
		Product product = findByCategoryAndNameAndType(request.getCategory(), request.getProductName(),
				request.getType());
		if (null != product) {
			product.setQauntity(product.getQauntity() + request.getQuantity());
		} else {
			product = productMapper.mapProductRequest(request);
			product.setCode(productUtil.getProductCode());
			product.setQauntity(1);
		}
		Product saveProduct = productRepository.save(product);
		return productMapper.mapProductResponse(saveProduct);

	}

	@Override
	public List<ProductResponse> findProdctByName(String name) {
		List<ProductResponse> responseList = productMapper.getAllProducts(productRepository.findByNameContaining(name).orElseThrow(() -> new ProductNotFoundException("Product with containing name [" + name + "] doesn't exist")));
		return responseList;
	}

	@Override
	public Product findByCategoryAndNameAndType(String category, String name, String type) {
		Product product = productRepository.findByCategoryAndNameAndType(category, name, type);
		return product;
	}

	@Override
	public List<ProductResponse> findProductByCategory(String category) {
		List<ProductResponse> responseList = productMapper.getAllProducts(productRepository.findByCategory(category));
		return responseList;
	}

	@Override
	public Page<Product> findAllUsingPagination(Pageable pageable) {
		Page<Product> products = productRepository.findAll(pageable);
		if (products.isEmpty())
			throw new ProductNotFoundException("Product list is empty");
		return products;
	}

	@Override
	public ProductResponse updateProduct(ProductRequest request) {

		Product product = findByCategoryAndNameAndType(request.getCategory(), request.getProductName(),
				request.getType());
		Product saveProduct = null;
		if (product.getQauntity() != 0) {
			if (null != product) {
				product.setQauntity(product.getQauntity() - request.getQuantity());
			}
			saveProduct = productRepository.save(product);

		}
		return productMapper.mapProductResponse(saveProduct);

	}

}
