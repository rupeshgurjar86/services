package com.mykart.product.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mykart.product.dto.OrderInfo;
@FeignClient(name = "order-service", url = "http://localhost:8084")
public interface OrderFeignService {
	@PostMapping("/orders")
	public ResponseEntity<Object> createOrder(@RequestBody OrderInfo orderInfo);
}
