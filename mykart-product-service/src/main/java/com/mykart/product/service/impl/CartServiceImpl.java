package com.mykart.product.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mykart.product.entity.Cart;
import com.mykart.product.mapper.CartMapper;
import com.mykart.product.repository.CartRepository;
import com.mykart.product.request.CartRequest;
import com.mykart.product.response.CartResponse;
import com.mykart.product.response.ProductResponse;
import com.mykart.product.service.CartService;

@Service
@Transactional
public class CartServiceImpl implements CartService {

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private CartMapper cartMapper;

	public CartResponse addToCart(CartRequest request) {
		Cart cart = cartRepository.findByCustomerIdAndCode(request.getCustomerId(), request.getProductId());
		Cart newCart = cartMapper.mapCartRequest(request, cart);
		return cartMapper.mapCartResponse(cartRepository.save(newCart));
	}

	@Override
	public void removeFromCart(long customerId, long productId) {
		cartRepository.deleteByCustomerIdAndCode(customerId, productId);
	}

	@Override
	public ProductResponse findByOrderId(long productId) {
		return null;
	}

	@Override
	public List<CartResponse> findAllItems(long customerId) {
		List<Cart> cartList = cartRepository.findAllByCustomerId(customerId);
		List<CartResponse> response  = cartMapper.mapCartResponseList(cartList);
		return response;
	}

	@Override
	public void removeFromCart(long customerId) {
		cartRepository.deleteByCustomerId(customerId);
	}

	@Override
	public CartResponse findByCustomerAndProductId(long customerId, long code) {
		Cart cart = cartRepository.findByCustomerIdAndCode(customerId, code);
		CartResponse response = cartMapper.mapCartResponse(cart);
		return response;
	}

}
