package com.mykart.product.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mykart.product.entity.Product;
import com.mykart.product.request.ProductRequest;
import com.mykart.product.response.ProductResponse;

public interface ProductService {

	List<ProductResponse> findAllProducts();

	ProductResponse findProductByCode(String code);

	ProductResponse save(ProductRequest request);

	List<ProductResponse> findProdctByName(String name);
	
	Product findByCategoryAndNameAndType(String category,String name,String type);

	List<ProductResponse> findProductByCategory(String category);

	Page<Product> findAllUsingPagination(Pageable pageable);
	
	ProductResponse updateProduct(ProductRequest request);
}
