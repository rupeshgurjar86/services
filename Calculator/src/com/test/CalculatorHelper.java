package com.test;

import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.params.provider.Arguments;

public class CalculatorHelper {

	public static Stream<Arguments> parameterizedList(){
		return Stream.of(
				Arguments.of("demo",2,Arrays.asList("1","2","3"))
				);
		
	}
	public  static Stream<Arguments> provideParameters(){
		return Stream.of(
				Arguments.of("Orange",2),
				Arguments.of("Kiwi",3)
				);
	}
	public  static Stream<Arguments> provideParametersWithSplitString(){
		return Stream.of(
				Arguments.of("'Orange,black'",2),
				Arguments.of("'Kiwi,whiteW'",3)
				);
	}

}
