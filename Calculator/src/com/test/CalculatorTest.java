package com.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.calc.Calculator;

@TestInstance(Lifecycle.PER_CLASS)
//@TestMethodOrder(OrderAnnotation.class)
public class CalculatorTest {

	Calculator calc;

	@BeforeAll
	public void setUp() {
		calc = new Calculator();
	}

	// @Test
	@DisplayName("Addition")
	// @Order(3)
	public void addTest() {
		int result = calc.add(3, 5);
		assertEquals(8, result);
		System.out.println("1. Addition is called  : " + this);
	}

	// @RepeatedTest(1)
	// @Order(1)
	public void subTest() {
		int result = calc.sub(3, 5);
		assertEquals(-2, result);
		System.out.println("2. Substraction is called : " + this);
	}

	// @Test
	// @Order(2)
	public void mulTest() {
		int result = calc.mul(3, 5);
		assertEquals(15, result);
		System.out.println("3. Multiplication is called  : " + this);
	}

	// @Test
	public void divTest() {
		int result = calc.div(0, 5);
		assertEquals(0, result);
		System.out.println("4. Division/ is called : " + this);
	}

	// @Test
	public void divExceptionTest() {
		assertThrows(ArithmeticException.class, () -> calc.div(5, 0));
		System.out.println("5. divException is called  : " + this);
	}

	@Nested
	class NestedClass {
		// @Test
		public void emptyMethod() {
		}

		// @ParameterizedTest
		@ValueSource(strings = { "string1", "String2", "Strig3" })
		public void readData(String value) {
			System.out.println("Input read from value source: " + value);
		}

		// @ParameterizedTest
		@CsvSource(value = { "a,1", "b,2", "foo,3" })
		public void testParameters(String name, int value) {
			System.out.println("CSV data " + name + ", Value: " + value);

		}

		@ParameterizedTest
		@MethodSource("com.test.CalculatorHelper#parameterizedList")
		public void testParametersFromMethodList(String s, int i, List<String> list) {
			System.out.println("Strings: " + s + " & i: " + i);
			list.forEach(System.out::print);
		}

		@ParameterizedTest
		@MethodSource("com.test.CalculatorHelper#provideParameters")
		public void testParametersFromMethod(String name, int value) {
			System.out.println("methoda data " + name + ", value : " + value);

		}

		@ParameterizedTest
		@MethodSource("com.test.CalculatorHelper#provideParametersWithSplitString")
		public void testParametersFromMethodUsingDelimeters(@ConvertWith(StringArrayConvertor.class) String[] name,
				int value) {
			System.out.println("methoda data " + name[0] + "," + name[1]);
		}
	}
}
