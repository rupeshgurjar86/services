package com.test;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class StringArrayConvertor extends SimpleArgumentConverter{

	@Override
	protected Object convert(Object source, Class<?> target) throws ArgumentConversionException {
		if(source instanceof String && String[].class.isAssignableFrom(target))
					return source.toString().split("\\s*,\\s*");
		else
			throw new IllegalArgumentException("Conversion from "+ source +" to "+target+" is not supported!!!");
			
	}

}
