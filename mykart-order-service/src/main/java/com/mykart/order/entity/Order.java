package com.mykart.order.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "orders")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_id")
	private Long id;

	private long customerId;

	private String customerEmail;

	private String customerAddress;
	
	private LocalDateTime orderCreatedDate;
	
	private BigDecimal totalPrice;
 
	private String status;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "item_details", joinColumns = { @JoinColumn(name = "order_id") })
	private List<OrderItem> items;

	public Order(long customerId, String customerEmail, String customerAddress, List<OrderItem> items) {
		super();
		this.customerId = customerId;
		this.customerEmail = customerEmail;
		this.customerAddress = customerAddress;
		this.items = items;
	}

	public Order(long customerId, String customerEmail, String customerAddress, LocalDateTime orderCreatedDate,
			BigDecimal totalPrice, List<OrderItem> items) {
		super();
		this.customerId = customerId;
		this.customerEmail = customerEmail;
		this.customerAddress = customerAddress;
		this.orderCreatedDate = orderCreatedDate;
		this.totalPrice = totalPrice;
		this.items = items;
	}

}
