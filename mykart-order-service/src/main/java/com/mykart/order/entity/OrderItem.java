package com.mykart.order.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "order_items")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long productId;
	private int quantity;
	private BigDecimal productPrice;
	private Long customerId;
	private double price;

	public OrderItem(Long productId, int quantity, BigDecimal productPrice, Long customerId, double price) {
		this.productId = productId;
		this.quantity = quantity;
		this.productPrice = productPrice;
		this.customerId = customerId;
		this.price = price;
	}

}
