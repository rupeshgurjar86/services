package com.mykart.order.exception;

public class InSufficientFundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InSufficientFundException() {
		super("Request data is not valid");
	}

	public InSufficientFundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InSufficientFundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InSufficientFundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InSufficientFundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
