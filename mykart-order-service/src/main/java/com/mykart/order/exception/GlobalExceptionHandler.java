package com.mykart.order.exception;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import feign.FeignException;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", LocalDateTime.now());
		String error = ex.getMessage();
		// ex.printStackTrace();

		// System.out.println("Error : "+ex);
		// String[] s1 = error.split(":");
		// body.put("ErrorMessge", errorMessage);
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(FeignException.class)
	public ResponseEntity<Object> handleDataNotFoundException(FeignException ex, WebRequest request) {
		
		HttpStatus status = HttpStatus.resolve(ex.status());
		String errorMsg = null;
		if (null == status) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			errorMsg = " service is down. Please try after sometime";
		} else {
			errorMsg = new String(ex.content());
		}
		Set<String> body = new HashSet<>();
		body.add(errorMsg);
		return new ResponseEntity<>(errorMsg, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(InSufficientFundException.class)
	public ResponseEntity<Object> handleInSufficientFundException(InSufficientFundException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Set<String> body = new HashSet<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			body.add(error.getDefaultMessage());
		}

		ErrorResponse error = new ErrorResponse("Validation Failed", body);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

}
