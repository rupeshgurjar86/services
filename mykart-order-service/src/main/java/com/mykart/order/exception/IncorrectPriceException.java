package com.mykart.order.exception;

public class IncorrectPriceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public IncorrectPriceException() {
	}

	public IncorrectPriceException(String message) {
		super(message);
	}

	public IncorrectPriceException(Throwable cause) {
		super(cause);
	}

	public IncorrectPriceException(String message, Throwable cause) {
		super(message, cause);
	}

	public IncorrectPriceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
