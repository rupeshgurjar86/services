package com.mykart.order.service;

import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mykart.order.dto.FundInfo;
import com.mykart.order.entity.Order;
import com.mykart.order.request.OrderRequest;
import com.mykart.order.response.OrderResponse;

public interface OrderService {
	OrderResponse save(OrderRequest request);
	OrderResponse findOrderById(Long id);
    Page<Order> getAllOrders(long customerId,Pageable pageable);
    String makePayment(FundInfo fundInfo);
    Page<Order> getAllOrdersByDate(long customerId, LocalDateTime date, Pageable pageable);
    Page<Order> getAllOrdersBetweenDates(long customerId, LocalDateTime date1,LocalDateTime date2, Pageable pageable);
}
