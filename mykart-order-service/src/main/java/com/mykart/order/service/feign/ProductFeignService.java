package com.mykart.order.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.mykart.order.response.ProductResponse;

@FeignClient(url = "http://localhost:8083/products",name = "product-service")
public interface ProductFeignService {
	  @GetMapping("/{code}")
	  public ProductResponse productByCode(@PathVariable String code);
}
