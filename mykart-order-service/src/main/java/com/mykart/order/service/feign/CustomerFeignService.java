package com.mykart.order.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.mykart.order.dto.Customer;

@FeignClient(url = "http://localhost:8082/customers",name = "customer-service")
public interface CustomerFeignService {
	@GetMapping(value = "/{customerId}")
	public Customer findCustomer(@PathVariable("customerId") long customerId);
}
