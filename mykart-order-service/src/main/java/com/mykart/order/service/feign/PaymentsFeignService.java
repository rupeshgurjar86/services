package com.mykart.order.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mykart.order.dto.OtherFundInfo;
import com.mykart.order.exception.InSufficientFundException;

@FeignClient(url = "http://localhost:8080/transfer",name = "bank-app")
public interface PaymentsFeignService {
	@PostMapping(value = "/others")
	public ResponseEntity<Object> fundTransfer(@RequestBody OtherFundInfo fund)throws InSufficientFundException;
}
