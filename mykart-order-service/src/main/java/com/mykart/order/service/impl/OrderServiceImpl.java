package com.mykart.order.service.impl;

import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mykart.order.dto.FundInfo;
import com.mykart.order.dto.OtherFundInfo;
import com.mykart.order.entity.Order;
import com.mykart.order.exception.ProductNotFoundException;
import com.mykart.order.mapper.OrderMapper;
import com.mykart.order.repository.OrderRepository;
import com.mykart.order.request.OrderRequest;
import com.mykart.order.response.OrderResponse;
import com.mykart.order.service.OrderService;
import com.mykart.order.service.feign.PaymentsFeignService;
import com.mykart.order.service.feign.ProductFeignService;
import com.mykart.order.util.OrderUtil;

import feign.FeignException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {
	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private ProductFeignService productFeignService;

	@Autowired
	private OrderMapper orderMapper;

	@Autowired
	private OrderUtil orderUtil;

	@Autowired
	private PaymentsFeignService paymentsFeignService;

	@Override
	public OrderResponse save(OrderRequest request) {
		OrderResponse checkPending = findByCustomerIdAndStatus(request.getCustomerId(), "Pending");
		if (checkPending.getCustomerId() != 0)
			throw new ProductNotFoundException("Please process pending order first.");
		Order order = orderRepository.save(orderMapper.mapOrderRequest(request));
		OrderResponse response = orderMapper.mapOrderResponse(order);
		return response;
	}

	@Override
	public OrderResponse findOrderById(Long id) {
		Order order = orderRepository.findById(id).orElseThrow(() -> new ProductNotFoundException("Order not found!!"));
		OrderResponse response = orderMapper.mapOrderResponse(order);
		return response;
	}

	@Override
	public Page<Order> getAllOrders(long customerId, Pageable pageable) {
		Page<Order> page = orderRepository.findByCustomerId(customerId, pageable);

		if (page.isEmpty())
			throw new ProductNotFoundException("Order list is empty");
		return page;
	}

	@Override
	public String makePayment(FundInfo fundInfo) {
		String status = findOrderById(fundInfo.getOrderId()).getStatus();
		if (status.equalsIgnoreCase("Processed"))
			throw new ProductNotFoundException("No Pending Order !!");

		OtherFundInfo otherFundInfo = new OtherFundInfo(fundInfo.getBeneficiary(),fundInfo.getAmount(), fundInfo.getTo(), fundInfo.getFrom());
		ResponseEntity<Object> result = paymentsFeignService.fundTransfer(otherFundInfo);
		updateOrderDetails(fundInfo.getCustomerId(), fundInfo.getOrderId());

		return result.getBody().toString();
	}

	@CircuitBreaker(name = "productservice", fallbackMethod = "fallbackProduct")
	public void findByProductCode(String code) {
		productFeignService.productByCode(code);
	}

	private void updateOrderDetails(long customerId, long orderId) {
		Order order = orderRepository.findByCustomerIdAndId(customerId, orderId);
		order.setStatus("Processed");
		orderRepository.save(order);
	}

	private OrderResponse findByCustomerIdAndStatus(long customerId, String status) {
		Order order = orderRepository.findByCustomerIdAndStatus(customerId, status);
		return orderMapper.mapOrderResponse(order);
	}

	@Override
	public Page<Order> getAllOrdersByDate(long customerId, LocalDateTime date, Pageable pageable) {
		Page<Order> order = orderRepository.findByCustomerIdAndOrderCreatedDateLessThanEqual(customerId, date,
				pageable);
		if (order.isEmpty())
			throw new ProductNotFoundException("Order list is empty");
		return order;
	}

	private ResponseEntity<Object> fallbackProduct(FeignException ex) {
		return orderUtil.parseException(ex, "Order");
	}

	@Override
	public Page<Order> getAllOrdersBetweenDates(long customerId, LocalDateTime date1, LocalDateTime date2,
			Pageable pageable) {
		
		Page<Order> order = orderRepository.findByCustomerIdAndOrderCreatedDateBetween(customerId, date1, date2,
				pageable);
		if (order.isEmpty())
			throw new ProductNotFoundException("Order list is empty");
		return order;
	}

}
