package com.mykart.order.repository;
import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mykart.order.entity.Order;
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
	Page<Order> findByCustomerId(long customerId,Pageable pageable);
	Order findByCustomerIdAndStatus(long customerId,String status);
	Order findByCustomerIdAndId(long customerId,long orderId);
	Page<Order> findByCustomerIdAndOrderCreatedDateLessThanEqual(long customerId,LocalDateTime orderCreatedDate,Pageable pageable);
	Page<Order> findByCustomerIdAndOrderCreatedDateBetween(long customerId,LocalDateTime startDate,LocalDateTime endDate,Pageable pageable);
}
