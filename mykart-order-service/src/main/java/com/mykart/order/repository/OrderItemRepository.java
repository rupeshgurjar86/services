package com.mykart.order.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mykart.order.entity.OrderItem;
@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
	Optional<OrderItem> findByProductId(long productid);
	Optional<List<OrderItem>> findByCustomerId(long productid);
	Optional<String> removeByCustomerIdAndProductId(long customerId,long productId);

}
