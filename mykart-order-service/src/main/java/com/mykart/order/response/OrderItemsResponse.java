package com.mykart.order.response;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderItemsResponse {
	private double productPrice;
	private String name;
	private String description;
	private String category;
	private String type;
	private int quantity;
	private BigDecimal price;
	/*
	 * public BigDecimal getPrice() { return productPrice.multiply(new
	 * BigDecimal(quantity)); }
	 */
}
