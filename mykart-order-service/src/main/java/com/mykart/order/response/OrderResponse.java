package com.mykart.order.response;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderResponse {
	private String orderId;
	private long customerId;
	private String customerEmail;
	private String customerAddress;
	private LocalDateTime orderDate;
	private BigDecimal totalAmount;
	private String status;
	private List<OrderItemsResponse> items;
}
