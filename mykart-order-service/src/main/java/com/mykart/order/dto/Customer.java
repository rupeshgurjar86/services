package com.mykart.order.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Customer {
	private Long customerId;
	private String username;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String address;
	private String accountNumber;
}
