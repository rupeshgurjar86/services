package com.mykart.order.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OtherFundInfo {
	private String beneficiary;
	private BigDecimal amount;
	private String to;
	private int from;
}
