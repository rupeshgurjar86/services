package com.mykart.order.mapper;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.mykart.order.dto.Customer;
import com.mykart.order.dto.FundInfo;
import com.mykart.order.dto.FundTransferInfo;
import com.mykart.order.entity.Order;
import com.mykart.order.entity.OrderItem;
import com.mykart.order.request.OrderItemsRequest;
import com.mykart.order.request.OrderRequest;
import com.mykart.order.response.OrderItemsResponse;
import com.mykart.order.response.OrderResponse;
import com.mykart.order.response.ProductResponse;
import com.mykart.order.service.feign.CustomerFeignService;
import com.mykart.order.service.feign.ProductFeignService;

@Component
public class OrderMapper {

	@Autowired
	private ProductFeignService productFeignService;

	@Autowired
	private CustomerFeignService customerFeignService;;

	@Autowired
	private OrderItemsMapper orderItemsMapper;

	BigDecimal totalPrice = new BigDecimal(0);

	private OrderResponse orderResponseMapper(Order o) {
		List<OrderItemsResponse> items = orderItemsMapper.mapOrderItemsResponse(o.getItems());
		return new OrderResponse(String.valueOf(o.getId()), o.getCustomerId(), o.getCustomerEmail(),
				o.getCustomerAddress(), o.getOrderCreatedDate(), o.getTotalPrice(), o.getStatus(), items);
	}

	public List<OrderResponse> mapOrderResponseList(List<Order> orders) {
		List<OrderResponse> orderResponseList = orders.stream().map(o -> orderResponseMapper(o))
				.collect(Collectors.toList());
		return orderResponseList;
	}

	public Map<String, Object> getOrderResponseList(Page<Order> pages) {
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = pages.getContent();
		List<OrderResponse> orderResponseList = mapOrderResponseList(orders);
		response.put("orders", orderResponseList);
		response.put("currentPage", pages.getNumber());
		response.put("totalItems", pages.getTotalElements());
		response.put("totalPages", pages.getTotalPages());
		return response;
	}

	private OrderItem orderItemMapper(OrderItemsRequest request, OrderRequest orderRequest) {
		productFeignService.productByCode(String.valueOf(request.getProductId()));
		BigDecimal price = request.getProductPrice().multiply(new BigDecimal(request.getQuantity()));
		OrderItem item = new OrderItem(request.getProductId(), request.getQuantity(), request.getProductPrice(),
				orderRequest.getCustomerId(), price.doubleValue());
		totalPrice = totalPrice.add(request.getPrice());
		return item;
	}

	public Order mapOrderRequest(OrderRequest orderRequest) {
		List<OrderItem> orderItemList = orderRequest.getItems().stream()
				.map(orderItem -> orderItemMapper(orderItem, orderRequest)).collect(Collectors.toList());
		Order order = new Order(orderRequest.getCustomerId(), orderRequest.getCustomerEmail(),
				orderRequest.getCustomerAddress(), LocalDateTime.now(), totalPrice, orderItemList);
		order.setStatus("Pending");
		return order;
	}

	public OrderResponse mapOrderResponse(Order order) {
		List<OrderItemsResponse> orderItemsList = orderItemsMapper.mapOrderItemsResponse(order.getItems());
		OrderResponse response = new OrderResponse(String.valueOf(order.getId()), order.getCustomerId(),
				order.getCustomerAddress(), order.getCustomerEmail(), order.getOrderCreatedDate(),
				order.getTotalPrice(), order.getStatus(), orderItemsList);
		return response;
	}

	public FundInfo getFundInfo(FundTransferInfo info) {
		Customer customer = customerFeignService.findCustomer(info.getCustomerId());
		FundInfo fundInfo = new FundInfo(info.getCustomerId(), info.getOrderId(), null, totalPrice, "MyKart App",
				Integer.valueOf(customer.getAccountNumber()));
		return fundInfo;
	}

}
