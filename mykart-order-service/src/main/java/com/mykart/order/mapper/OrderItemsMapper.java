package com.mykart.order.mapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mykart.order.entity.OrderItem;
import com.mykart.order.request.OrderItemsRequest;
import com.mykart.order.request.OrderRequest;
import com.mykart.order.response.OrderItemsResponse;
import com.mykart.order.response.ProductResponse;
import com.mykart.order.service.feign.CustomerFeignService;
import com.mykart.order.service.feign.ProductFeignService;

@Component
public class OrderItemsMapper {
	@Autowired
	ProductFeignService productFeignService;

	@Autowired
	CustomerFeignService customerFeignService;;

	public List<OrderItemsResponse> mapOrderItemsResponse(List<OrderItem> orderItemlist) {
		List<OrderItemsResponse> orderItemsResponseList = new ArrayList<OrderItemsResponse>();
		for (OrderItem orderItem : orderItemlist) {
			OrderItemsResponse orderItemsResponse = mapOrderItemsResponse(orderItem);
			orderItemsResponseList.add(orderItemsResponse);
		}
		return orderItemsResponseList;
	}

	
	public List<OrderItem> mapOrderItemsRequest(OrderRequest request) {
		List<OrderItem> orderItemList = new ArrayList<OrderItem>();
		BigDecimal totalPrice = new BigDecimal(0);

		for (OrderItemsRequest info : request.getItems()) {
			OrderItem orderItem = new OrderItem();
			orderItem.setCustomerId(request.getCustomerId());
			orderItem.setProductId(info.getProductId());
			productFeignService.productByCode(String.valueOf(info.getProductId()));
			BigDecimal price = info.getProductPrice().multiply(new BigDecimal(info.getQuantity()));
			orderItem.setProductPrice(price);
			orderItem.setQuantity(info.getQuantity());
			orderItemList.add(orderItem);
			totalPrice = totalPrice.add(info.getPrice());
		}
		return orderItemList;
	}
	public OrderItemsResponse mapOrderItemsResponse(OrderItem orderItem) {
		OrderItemsResponse orderItemsResponse = new OrderItemsResponse();
		ProductResponse product = productFeignService.productByCode(String.valueOf(orderItem.getProductId()));
		orderItemsResponse.setName(product.getProductName());
		orderItemsResponse.setCategory(product.getCategory());
		orderItemsResponse.setType(product.getType());
		orderItemsResponse.setProductPrice(product.getProductPrice());
		orderItemsResponse.setQuantity(orderItem.getQuantity());
		orderItemsResponse.setDescription(product.getDescription());
		orderItemsResponse.setPrice(BigDecimal.valueOf(orderItem.getPrice()));
		return orderItemsResponse;
	}
}
