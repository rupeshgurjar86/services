package com.mykart.order.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mykart.order.dto.Customer;
import com.mykart.order.dto.FundInfo;
import com.mykart.order.dto.FundTransferInfo;
import com.mykart.order.service.feign.CustomerFeignService;
@Component
public class FundTransferMapper {
	@Autowired
	CustomerFeignService customerFeignService;;

	public FundInfo getFundInfo(FundTransferInfo info) {
		FundInfo fundInfo = new FundInfo();
		fundInfo.setAmount(info.getAmount());
		Customer customer = customerFeignService.findCustomer(info.getCustomerId());
		fundInfo.setFrom(Integer.valueOf(customer.getAccountNumber()));
		fundInfo.setTo("MyKart App");
		fundInfo.setOrderId(info.getOrderId());
		fundInfo.setCustomerId(info.getCustomerId());
		return fundInfo;
	}

}
