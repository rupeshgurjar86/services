package com.mykart.order.request;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderRequest {
	@NotEmpty(message = "Customer ID can not be empty.")
	@NotNull(message = "Customer ID can not be null.")
	private long customerId;

	@NotEmpty(message = "Email  can not be empty.")
	@NotNull(message = "Email can not be null.")
	@Email(message = "Emai ID is not valid ")
	private String customerEmail;

	@NotEmpty(message = "Address can not be empty.")
	@NotNull(message = "Address can not be null.")
	private String customerAddress;

	@NotEmpty(message = "Order list can not be empty.")
	@NotNull(message = "Order list can not be null.")
	private List<OrderItemsRequest> items;
	
}
