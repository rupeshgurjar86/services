package com.mykart.order.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mykart.order.dto.FundInfo;
import com.mykart.order.dto.FundTransferInfo;
import com.mykart.order.entity.Order;
import com.mykart.order.mapper.FundTransferMapper;
import com.mykart.order.mapper.OrderMapper;
import com.mykart.order.request.OrderRequest;
import com.mykart.order.response.OrderResponse;
import com.mykart.order.service.OrderService;
import com.mykart.order.util.OrderUtil;

import feign.FeignException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@RequestMapping("/orders")
public class OrderController {
	private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);

	@Autowired
	private OrderService orderService;

	@Autowired
	private OrderUtil orderUtil;
	
	@Autowired
	private OrderMapper orderMapper;
	
	@Autowired
	private FundTransferMapper fundTransferMapper;

	@PostMapping("")
	@CircuitBreaker(name = "productservice", fallbackMethod = "fallbackProduct")
	public ResponseEntity<Object> createOrder(@RequestBody OrderRequest request) {
		LOG.debug("OrderController.createOrder request is processing");
		OrderResponse response = orderService.save(request);
		LOG.debug("OrderController.createOrder request is finish");
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@GetMapping("/order/{orderId}")
	public ResponseEntity<Object> findOrderById(@PathVariable("orderId") Long id) {
		LOG.debug("OrderController.findOrderById request is processing");
		OrderResponse response = orderService.findOrderById(id);
		LOG.debug("OrderController.findOrderById request is finish");
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@PostMapping("/payment")
	public ResponseEntity<Object> makePayment(@Valid @RequestBody FundTransferInfo fundTransferInfo) {
		LOG.debug("OrderController.makePayment request is processing");
		FundInfo fundInfo = fundTransferMapper.getFundInfo(fundTransferInfo);
		String result = orderService.makePayment(fundInfo);
		LOG.debug("OrderController.makePayment request is finish");
		return new ResponseEntity<Object>(result, HttpStatus.OK);
	}

	@GetMapping("/customer/{customerId}")
	public ResponseEntity<Object> findAll(@PathVariable("customerId") Long customerId,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
		LOG.debug("OrderController.findAll request is processing");

		Pageable paging = PageRequest.of(page, size);

		Page<Order> pages = orderService.getAllOrders(customerId, paging);
		Map<String, Object> response = orderMapper.getOrderResponseList(pages);

		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@GetMapping("/customer/{customerId}/date/{date}")
	public ResponseEntity<Object> findAllByDate(@PathVariable("customerId") Long customerId,
			@PathVariable("date") String date, @RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "3") int size) {
		LOG.debug("OrderController.findAllByDate request is processing");

		String str = date + " 23:59";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
		Pageable paging = PageRequest.of(page, size);

		Page<Order> pages = orderService.getAllOrdersByDate(customerId, dateTime, paging);
		Map<String, Object> response = orderMapper.getOrderResponseList(pages);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@GetMapping("/customer/{customerId}/start/{date1}/end/{date2}")
	public ResponseEntity<Object> findAllBetweenDates(@PathVariable("customerId") Long customerId,
			@PathVariable("date1") String date1,@PathVariable("date2") String date2, @RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "3") int size) {
		LOG.debug("OrderController.findAllByDate request is processing");

		String str1 = date1 + " 00:00";
		String str2 = date2 + " 23:59";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime dateTime1 = LocalDateTime.parse(str1, formatter);
		LocalDateTime dateTime2 = LocalDateTime.parse(str2, formatter);
		Pageable paging = PageRequest.of(page, size);

		Page<Order> pages = orderService.getAllOrdersBetweenDates(customerId, dateTime1,dateTime2, paging);
		Map<String, Object> response = orderMapper.getOrderResponseList(pages);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
	@GetMapping("/api")
	public String test() {
		return "Order service is up & running";
	}

	private ResponseEntity<Object> fallbackProduct(FeignException ex) {
		return orderUtil.parseException(ex, "Order");
	}
}
