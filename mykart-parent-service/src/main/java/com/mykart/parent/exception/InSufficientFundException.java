package com.mykart.parent.exception;

public class InSufficientFundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InSufficientFundException() {
		super("Insufficient fund to transfer.");
	}
}
