package com.mykart.parent.exception;

public class UserNotFoundException extends RuntimeException {
	public UserNotFoundException() {
		super("No Data Found!!!");
	}
}
