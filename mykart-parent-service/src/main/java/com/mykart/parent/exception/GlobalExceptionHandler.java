package com.mykart.parent.exception;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import feign.FeignException;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", LocalDateTime.now());
		String error = ex.getMessage();
		String[] s1 = error.split(":");
		String errorMessage = s1[3] + s1[4] + s1[5];
		// body.put("ErrorMessge", errorMessage);
		return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(FeignException.FeignClientException.class)
	public ResponseEntity<Object> handleFeignStatusException(FeignException e, HttpServletResponse response) {
		Set<String> body = new HashSet<>();
		body.add(e.contentUTF8());
		ErrorResponse error = new ErrorResponse(e.contentUTF8(), body);

		response.setContentType(e.getMessage());
		return new ResponseEntity<Object>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(FeignException.NotFound.class)
	public ResponseEntity<Object> handleFeignStatusNotFoundException(FeignException e, HttpServletResponse response) {
		Set<String> body = new HashSet<>();
		body.add(e.contentUTF8());
		ErrorResponse error = new ErrorResponse(e.contentUTF8(), body);

		response.setContentType(e.getMessage());
		return new ResponseEntity<Object>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(UserNameAlreadyExistException.class)
	public ResponseEntity<Object> handleCustomerNameAlreadyExistException(UserNameAlreadyExistException ex,
			WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.EXPECTATION_FAILED);
	}

	@ExceptionHandler(DataNotFoundException.class)
	public ResponseEntity<Object> handleDataNotFoundException(DataNotFoundException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.EXPECTATION_FAILED);
	}

	@ExceptionHandler(FeignException.Unauthorized.class)
	public ResponseEntity<Object> handleInvalidLoginCredentialsException(FeignException.Unauthorized ex,
			WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add("Invalid Username/Password");

		ErrorResponse error = new ErrorResponse(ex.getLocalizedMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(InSufficientFundException.class)
	public ResponseEntity<Object> handleInSufficientFundException(InSufficientFundException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add("Insufficient Fund");
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(EmailAlreadyExistException.class)
	public ResponseEntity<Object> handleEmailAlreadyExistException(EmailAlreadyExistException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<Object> handleCustomerNotFoundException(UserNotFoundException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Set<String> body = new HashSet<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			body.add(error.getDefaultMessage());
		}

		ErrorResponse error = new ErrorResponse("Validation Failed", body);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

}
