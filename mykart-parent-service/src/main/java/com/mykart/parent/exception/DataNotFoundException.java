package com.mykart.parent.exception;

public class DataNotFoundException extends RuntimeException {
	public DataNotFoundException() {
		super("No Data Found!!!");
	}
}
