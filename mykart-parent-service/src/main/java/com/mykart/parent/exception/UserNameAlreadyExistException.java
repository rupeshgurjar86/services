package com.mykart.parent.exception;

public class UserNameAlreadyExistException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNameAlreadyExistException() {
		super("Customername is already registered");
	}
}
