package com.mykart.parent.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {
	@Bean
	public Docket api() {
		List<ResponseMessage> responseMessages = new ArrayList<>();
		ResponseMessage response500 = new ResponseMessageBuilder().code(500).message("Server Error").build();
		ResponseMessage response401 = new ResponseMessageBuilder().code(401).message("Invalid login & credentials").build();
		ResponseMessage response404 = new ResponseMessageBuilder().code(402).message("No data found").build();
		ResponseMessage response417 = new ResponseMessageBuilder().code(417).message("User name already exist.").build();
		ResponseMessage response400 = new ResponseMessageBuilder().code(417).message("Invalid username/password.").build();
		responseMessages.add(response500);
		responseMessages.add(response401);
		responseMessages.add(response404);
		responseMessages.add(response417);
		
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.mykart.parent.controller"))
				.paths(PathSelectors.ant("/*/**"))
				.build()
				.apiInfo(apiInfo())
				.useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.GET, responseMessages);
	}
	private ApiInfo apiInfo() {
		 return new ApiInfo(
			      "Bank App REST API", 
			      "Some custom description of API.", 
			      "API TOS", 
			      "Terms of service", 
			      new Contact("Rupesh Gurjar", "https://github.com/rupeshgurjar/spring-crud", "rupeshgurjar86@gmail.com"), 
			      "License of API", "API license URL", Collections.emptyList());
	}
}