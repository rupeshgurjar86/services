package com.mykart.parent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MykartParentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MykartParentServiceApplication.class, args);
	}
}
