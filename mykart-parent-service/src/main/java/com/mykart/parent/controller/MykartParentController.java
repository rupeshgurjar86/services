package com.mykart.parent.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mykart.parent.dto.CartInfo;
import com.mykart.parent.dto.FundTransferInfo;
import com.mykart.parent.dto.LoginInfo;
import com.mykart.parent.dto.OrderInfo;
import com.mykart.parent.dto.OrderItemInfo;
import com.mykart.parent.dto.ProductInfo;
import com.mykart.parent.dto.RegistrationInfo;
import com.mykart.parent.service.CustomerProxyService;
import com.mykart.parent.service.OrderProxyService;
import com.mykart.parent.service.ProductProxyService;
import com.mykart.parent.util.ParentUtil;

import feign.FeignException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@RequestMapping("/mykart")
public class MykartParentController {
	private static final Logger LOG = LoggerFactory.getLogger(MykartParentController.class);

	@Autowired
	private CustomerProxyService customerProxyService;

	@Autowired
	private ProductProxyService productProxyService;

	@Autowired
	private OrderProxyService orderProxyService;
	
	@Autowired
	private ParentUtil parentUtil;

	@PostMapping(value = "/customer/signup")
	@CircuitBreaker(name = "customerservice", fallbackMethod = "fallbackCustomer")
	public ResponseEntity<Object> userRegistration(@RequestBody RegistrationInfo user) {
		LOG.info("Registration Result====", user);
		customerProxyService.signup(user);
		LOG.info("End---MykartParentController().userRegistration()");
		return new ResponseEntity<Object>("User registration completed succefully", HttpStatus.OK);
	}

	@PostMapping(value = "/customer/login")
	@CircuitBreaker(name = "customerservice", fallbackMethod = "fallbackCustomer")
	public ResponseEntity<Object> login(@RequestBody LoginInfo user) {
		LOG.info("Begin---MykartParentController().login()");
		ResponseEntity<Object> result = customerProxyService.login(user);
		LOG.info("End---MykartParentController().login()" + result.getBody());

		return new ResponseEntity<Object>(result.getBody(), HttpStatus.OK);
	}

	@GetMapping("/products")
	@CircuitBreaker(name = "productservice", fallbackMethod = "fallbackProduct")
	public ResponseEntity<Object> allProducts() {
		List<ProductInfo> productList = productProxyService.allProducts().getBody();
		return new ResponseEntity<Object>(productList, HttpStatus.OK);
	}

	@GetMapping("/products/code/{code}")
	@CircuitBreaker(name = "productservice", fallbackMethod = "fallbackProduct")
	public ResponseEntity<Object> productByCode(@PathVariable String code) {
		ProductInfo productInfo = productProxyService.productByCode(code).getBody();
		return new ResponseEntity<Object>(productInfo, HttpStatus.OK);
	}

	@PostMapping("/products")
	@CircuitBreaker(name = "productservice", fallbackMethod = "fallbackProduct")
	public ResponseEntity<Object> addProduct(@RequestBody ProductInfo productInfo) {
		productProxyService.addProduct(productInfo);
		return new ResponseEntity<Object>("Product added successfully...", HttpStatus.OK);
	}

	@GetMapping("/products/name/{name}")
	@CircuitBreaker(name = "productservice", fallbackMethod = "fallbackProduct")
	public ResponseEntity<Object> allProductsByName(@PathVariable("name") String name) {
		List<ProductInfo> productList = productProxyService.allProductsByName(name).getBody();
		return new ResponseEntity<Object>(productList, HttpStatus.OK);

	}

	// @PostMapping("/items")
	public ResponseEntity<Object> addToCart(OrderItemInfo orderItemInfo) {
		orderProxyService.addToCart(orderItemInfo);
		return new ResponseEntity<Object>("Item added to the cart", HttpStatus.OK);
	};

//	@DeleteMapping("/items/{customerId}/{productId}")
	public ResponseEntity<Object> removeFromCart(@PathVariable long customerId, @PathVariable long productId) {
		orderProxyService.removeFromCart(customerId, productId);
		return new ResponseEntity<Object>("Item removed from the cart", HttpStatus.OK);
	};

	// @GetMapping("/items/{customerId}")
	public ResponseEntity<Object> getAllItems(@PathVariable long customerId) {
		List<OrderInfo> orderInfoList = orderProxyService.getAllItems(customerId).getBody();
		return new ResponseEntity<Object>(orderInfoList, HttpStatus.OK);
	};

	// @PostMapping("/items/order")
	public ResponseEntity<Object> placeOrder(@RequestBody OrderInfo order) {
		orderProxyService.placeOrder(order);
		return new ResponseEntity<Object>("Order placed successfully", HttpStatus.OK);
	};

	@PostMapping("/orders")
	@CircuitBreaker(name = "orderservice", fallbackMethod = "fallbackOrder")
	public ResponseEntity<Object> createOrder(@RequestBody OrderInfo orderInfo) {
		ResponseEntity<Object> order = orderProxyService.createOrder(orderInfo);
		return new ResponseEntity<Object>(order.getBody(), HttpStatus.OK);
	};

	@GetMapping("/orders/order/{orderId}")
	@CircuitBreaker(name = "orderservice", fallbackMethod = "fallbackOrder")
	public ResponseEntity<Object> findOrderById(@PathVariable("orderId") Long id) {
		ResponseEntity<Object> object = orderProxyService.findOrderById(id);
		return new ResponseEntity<Object>(object.getBody(), HttpStatus.OK);
	};

	@CircuitBreaker(name = "orderservice", fallbackMethod = "fallbackOrder")
	@PostMapping("/orders/payment")
	public ResponseEntity<Object> makePayment(@RequestBody FundTransferInfo fundInfo) {
		ResponseEntity<Object> response = orderProxyService.makePayment(fundInfo);
		return new ResponseEntity<Object>(response.getBody(), HttpStatus.OK);
	};

	@CircuitBreaker(name = "orderservice", fallbackMethod = "fallbackOrder")
	@GetMapping("/orders/customer/{customerId}")
	public ResponseEntity<Object> findAll(@PathVariable("customerId") Long customerId,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
		ResponseEntity<Object> object = orderProxyService.findAll(customerId, page, size);
		return new ResponseEntity<Object>(object.getBody(), HttpStatus.OK);
	};

	@PostMapping("/cart")
	@CircuitBreaker(name = "productservice", fallbackMethod = "fallbackProduct")
	public ResponseEntity<Object> addToCart(@RequestBody CartInfo cartInfo) {
		ResponseEntity<Object> response = productProxyService.addToCart(cartInfo);
		return new ResponseEntity<Object>(response.getBody(), HttpStatus.OK);

	};

	@PostMapping("/cart/orders")
	@CircuitBreaker(name = "productservice", fallbackMethod = "fallbackProduct")
	public ResponseEntity<Object> placeOrderFromCart(@RequestBody OrderInfo orderInfo) {
		ResponseEntity<Object> response = productProxyService.placeOrder(orderInfo);
		return new ResponseEntity<Object>(response.getBody(), HttpStatus.OK);

	};

	private ResponseEntity<Object> fallbackProduct(FeignException ex) {
		return parentUtil.parseException(ex,"Product");
	}

	private ResponseEntity<Object> fallbackCustomer(FeignException ex) {
		return parentUtil.parseException(ex,"Customer");
	}

	private ResponseEntity<Object> fallbackOrder(FeignException ex) {
		return parentUtil.parseException(ex,"Order");

	}
}
