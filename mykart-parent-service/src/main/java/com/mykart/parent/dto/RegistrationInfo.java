package com.mykart.parent.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RegistrationInfo {
	@NotEmpty(message = "Username can not be empty.")
	@NotNull(message = "Username can not be null.")
	private String username;

	@NotEmpty(message = "Password can not be empty.")
	@NotNull(message = "Username can not be null.")
	@Size(min = 6, max = 20)
	private String password;

	@NotEmpty(message = "First name can not be empty.")
	@NotNull(message = "Username can not be null.")
	private String firstName;

	@NotEmpty(message = "Last name can not be empty.")
	@NotNull(message = "Username can not be null.")
	private String lastName;

	@NotEmpty(message = "Email can not be empty.")
	@Email(message = "Email is not valid")
	private String email;

	@NotEmpty(message = "Phone number can not be empty.")
	@Size(message ="Please enter 10 digit mobile number.", min = 10 ,max = 10)
	private String phone;


	@NotEmpty(message = "Address can not be empty.")
	private String address;

}
