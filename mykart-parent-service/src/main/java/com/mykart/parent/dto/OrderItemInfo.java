package com.mykart.parent.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderItemInfo {
	private Long productId;
	private int quantity;
	private BigDecimal productPrice;
	private Long customerId;

	public BigDecimal getPrice() {
		return productPrice.multiply(new BigDecimal(quantity));
	}

}
