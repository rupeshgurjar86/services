package com.mykart.parent.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class SavingsTransactionInfo {
	private LocalDateTime date;
	private String description;
	private String status;
	private double amount;
	private int baneficiaryaccount;
}
