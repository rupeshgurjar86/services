package com.mykart.parent.dto;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerAccountInfo {
	private String customername;
	private String firstName;
	private String lastName;
	private int accountNumber;
	private BigDecimal accountBalance;
}
