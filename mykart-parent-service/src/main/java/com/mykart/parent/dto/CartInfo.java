package com.mykart.parent.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartInfo {
	private long customerId;
	private long productId;
	private String name;
	private String description;
	private BigDecimal price;
	private int quantity;
}
