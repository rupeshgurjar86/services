package com.mykart.parent.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfo {
	private String name;
	private String description;
	private double price;
	private String category;
	private String type;
	private int quantity;
}
