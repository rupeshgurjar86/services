package com.mykart.parent.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginInfo {
   
	@NotEmpty(message = "Username can not be empty.")
	private String username;
	@NotEmpty(message = "Password can not be empty.")
	private String password;
}
