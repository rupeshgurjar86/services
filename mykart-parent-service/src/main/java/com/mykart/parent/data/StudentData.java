package com.mykart.parent.data;

import java.util.ArrayList;
import java.util.List;

import com.mykart.parent.model.User;

public class StudentData {
	static List<User> studentList = new ArrayList<User>();
 	public static List<User> getStudentData() {
		if(studentList.isEmpty()||studentList.size()==0) {
			studentList.add(new User(1, "Raj"));
			studentList.add(new User(2, "Rahul"));
			studentList.add(new User(3, "Amar"));
			studentList.add(new User(4, "Kaushal"));
			studentList.add(new User(5, "Golu"));
		}
		return studentList;
	}
}
