package com.mykart.parent.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mykart.parent.dto.CartInfo;
import com.mykart.parent.dto.OrderInfo;
import com.mykart.parent.dto.ProductInfo;

@FeignClient(name = "product-service")
public interface ProductProxyService {

	@GetMapping("/products")
	public ResponseEntity<List<ProductInfo>> allProducts();

	@GetMapping("/products/{code}")
	public ResponseEntity<ProductInfo> productByCode(@PathVariable String code);

	@PostMapping("/products")
	public ResponseEntity<ProductInfo> addProduct(@RequestBody ProductInfo productInfo);
	
	@GetMapping("/products/name/{name}")
	public ResponseEntity<List<ProductInfo>> allProductsByName(@PathVariable("name") String name) ;
	
	@PostMapping("/cart")
	public ResponseEntity<Object> addToCart(@RequestBody CartInfo cartInfo) ;

	@PostMapping("/cart/orders")
	public ResponseEntity<Object> placeOrder(@RequestBody OrderInfo orderInfo);

}
