package com.mykart.parent.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.mykart.parent.dto.FundTransferInfo;
import com.mykart.parent.dto.OrderInfo;
import com.mykart.parent.dto.OrderItemInfo;

@FeignClient(name = "order-service")
public interface OrderProxyService {
	@PostMapping("/items")
	public ResponseEntity<Object> addToCart(@RequestBody OrderItemInfo orderItemInfo);

	@DeleteMapping("/items/{customerId}/{productId}")
	public ResponseEntity<Object> removeFromCart(@PathVariable long customerId, @PathVariable long productId);

	@GetMapping("/items/{customerId}")
	public ResponseEntity<List<OrderInfo>> getAllItems(@PathVariable long customerId);

	@PostMapping("/items/order")
	public ResponseEntity<Object> placeOrder(@RequestBody OrderInfo order);
	
	@PostMapping("/orders")
	public ResponseEntity<Object> createOrder(@RequestBody OrderInfo orderInfo) ;
	@GetMapping("/orders/order/{orderId}")
	public ResponseEntity<Object> findOrderById(@PathVariable("orderId") Long id) ;

	@PostMapping("/orders/payment")
	public ResponseEntity<Object> makePayment(@RequestBody FundTransferInfo fundInfo);

	@GetMapping("/orders/customer/{customerId}")
	public ResponseEntity<Object> findAll(@PathVariable("customerId") Long customerId,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size);
	

}
