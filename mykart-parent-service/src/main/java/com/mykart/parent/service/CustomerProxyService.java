package com.mykart.parent.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mykart.parent.dto.LoginInfo;
import com.mykart.parent.dto.RegistrationInfo;
@FeignClient(name = "customer-service")
public interface CustomerProxyService {

	@PostMapping(value = "/customers/signup")
	public ResponseEntity<Object>  signup(@RequestBody RegistrationInfo user) ;

	@PostMapping(value = "/customers/login")
	public ResponseEntity<Object>  login(@RequestBody LoginInfo user);
	
}
