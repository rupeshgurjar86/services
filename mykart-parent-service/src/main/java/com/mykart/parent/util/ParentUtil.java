package com.mykart.parent.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import feign.FeignException;

@Component
public class ParentUtil {
	public ResponseEntity<Object> parseException(FeignException ex,String module) {
		HttpStatus status = HttpStatus.resolve(ex.status());
		String errorMsg = null;
		if (null == status) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			errorMsg = module+" service is down. Please try after sometime";
		} else {
			errorMsg = new String(ex.content());
		}
		return new ResponseEntity<Object>(errorMsg, status);

	}
}
