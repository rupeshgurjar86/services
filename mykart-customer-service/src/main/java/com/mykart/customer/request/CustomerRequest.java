package com.mykart.customer.request;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerRequest {
	@NotEmpty(message = "Username can not be empty.")
	@NotNull(message = "Username can not be null.")
	private String username;

	@NotEmpty(message = "Password can not be empty.")
	@NotNull(message = "Password can not be null.")
	@Size(min = 6,max=20,message = "Password must be 6-20 characters.")
	private String password;

	@NotEmpty(message = "First name can not be empty.")
	@NotNull(message = "First name can not be null.")
	private String firstName;

	@NotEmpty(message = "Last name can not be empty.")
	@NotNull(message = "Last name can not be null.")
	private String lastName;

	@Column(name = "email", nullable = false, unique = true)
	@NotEmpty(message = "Email can not be empty.")
	@Email(message = "Email is not valid")
	private String email;

	@NotEmpty(message = "Phone number can not be empty.")
	@Size(message ="Please enter 10 digit mobile number.", min = 10 ,max = 10)
	private String phone;
	
	@NotEmpty(message = "Address can not be empty.")
	private String address;

	@Getter
	@Setter
	public static class CustomerRegistrationRequest extends CustomerRequest{
		
	}
	@Getter
	@Setter
	public static class CustomerLoginRequest {
	   
		@NotEmpty(message = "Username can not be empty.")
		private String username;
		@NotEmpty(message = "Password can not be empty.")
		private String password;
	}
	
	@Getter
	@Setter
	public static class CustomerUpdateRequest extends CustomerRequest{
		@NotEmpty(message = "Account number can not be empty.")
		private String accountNumber;
	}

}
