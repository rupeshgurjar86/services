package com.mykart.customer.service;

import java.util.List;

import com.mykart.customer.entity.Customer;
import com.mykart.customer.request.CustomerRequest.CustomerLoginRequest;
import com.mykart.customer.request.CustomerRequest.CustomerRegistrationRequest;
import com.mykart.customer.request.CustomerRequest.CustomerUpdateRequest;
import com.mykart.customer.response.CustomerResponse;
import com.mykart.customer.response.CustomerResponse.CustomerLoginResponse;
import com.mykart.customer.response.CustomerResponse.CustomerRegistrationResponse;

public interface CustomerService {

	public Customer findByEmail(String email);

	public Customer findByCustomername(String customername);

	public CustomerRegistrationResponse createCustomer(CustomerRegistrationRequest request);

	public CustomerResponse updateCustomer(CustomerUpdateRequest customer);

	public boolean checkCustomerExists(String customername, String email);

	public boolean checkCustomernameExists(String customername);

	public boolean checkEmailExists(String email);

	public CustomerLoginResponse login(CustomerLoginRequest request);

	public List<Customer> findCustomerList();

	public Customer findByCustomernameAndPassword(String customername, String password);

	public Customer findByCustomernameOrEmail(String customername, String email);

	public Customer findByCustomerId(long customerId);

}
