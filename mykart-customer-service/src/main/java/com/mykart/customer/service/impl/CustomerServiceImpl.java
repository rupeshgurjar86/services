package com.mykart.customer.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.mykart.customer.entity.Customer;
import com.mykart.customer.exception.InvalidLoginCredentialsException;
import com.mykart.customer.exception.UserNameAlreadyExistException;
import com.mykart.customer.exception.UserNotFoundException;
import com.mykart.customer.mapper.CustomerMapper;
import com.mykart.customer.repository.CustomerRepository;
import com.mykart.customer.request.CustomerRequest.CustomerLoginRequest;
import com.mykart.customer.request.CustomerRequest.CustomerRegistrationRequest;
import com.mykart.customer.request.CustomerRequest.CustomerUpdateRequest;
import com.mykart.customer.response.CustomerResponse;
import com.mykart.customer.response.CustomerResponse.CustomerLoginResponse;
import com.mykart.customer.response.CustomerResponse.CustomerRegistrationResponse;
import com.mykart.customer.service.CustomerService;
import com.mykart.customer.util.CustomerUtil;
import com.mykart.customer.validators.CustomerRegistrationValidator;

import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CustomerUtil customerUtil;

	@Autowired
	CustomerRegistrationValidator customerRegistrationValidator;
	
	@Autowired
	private CustomerMapper customerMapper;
	
   @Bean
	    public BCryptPasswordEncoder passwordEncoder() {
	        return new BCryptPasswordEncoder();
	    }

	public Customer findByCustomername(String customername) {
		return customerRepository.findByUsername(customername).orElseThrow(() -> new UserNotFoundException());
	}

	public Customer findByEmail(String email) {

		Customer customer = customerRepository.findByEmail(email).orElseThrow(() -> new UserNotFoundException());
		return customer;
	}

	public CustomerRegistrationResponse createCustomer(CustomerRegistrationRequest request) {
		
		Customer customerRequest = customerMapper.mapCustomerRegistrationRequest(request);
		customerRegistrationValidator.preProcessDataValidation(customerRequest, "create");
		customerRequest.setCustomerId(Long.valueOf(customerUtil.getNextCustomerId()));
		customerRequest.setPassword(passwordEncoder().encode(request.getPassword()));
		Customer customerResponse = customerRepository.save(customerRequest);
		CustomerRegistrationResponse customerRegistrationResponse = customerMapper.mapRegistrationResponse(customerResponse);		
		return customerRegistrationResponse;
	}

	public boolean checkCustomerExists(String customername, String email) {
		if (checkCustomernameExists(customername) || checkEmailExists(customername)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkCustomernameExists(String customername) {
		if (null == findByCustomername(customername)) {
			return true;
		} else {
			throw new UserNameAlreadyExistException();
		}
	}

	public boolean checkEmailExists(String email) {
		if (null != findByEmail(email)) {
			return true;
		}
		return false;
	}

	public CustomerResponse updateCustomer(CustomerUpdateRequest request) {
		Customer customerUpdateRequest = customerMapper.mapCustomerUpdateRequest(request);
		customerRegistrationValidator.preProcessDataValidation(customerUpdateRequest, "update");
		Customer customerUpdate = customerRegistrationValidator.getCustomer();
		customerUpdate.setAccountNumber(request.getAccountNumber());
		Customer updatedCustomer = customerRepository.save(customerUpdate);
		CustomerResponse customerResponseInfo = customerMapper.mapLoginResponse(updatedCustomer);
		return customerResponseInfo;
	}

	public List<Customer> findCustomerList() {
		return customerRepository.findAll();
	}

	@Override
	public CustomerLoginResponse login(CustomerLoginRequest request) {

		/*
		 * Customer customer =
		 * customerRepository.findByUsernameAndPassword(request.getUsername(),
		 * request.getPassword()) .orElseThrow(() -> new
		 * InvalidLoginCredentialsException());
		 */
		Customer customerPwd = customerRepository.findByUsername(request.getUsername())
				.orElseThrow(() -> new InvalidLoginCredentialsException());
		
		String encodedPassword = customerPwd.getPassword();
		boolean isPasswordMatch = passwordEncoder().matches(request.getPassword(), encodedPassword);
		if(!isPasswordMatch)
			throw new InvalidLoginCredentialsException();
	   CustomerLoginResponse customerLogingResponse = customerMapper.mapLoginResponse(customerPwd);

		return customerLogingResponse;
	}

	@Override
	public Customer findByCustomernameAndPassword(String customername, String password) {
		return customerRepository.findByUsernameAndPassword(customername, password)
				.orElseThrow(() -> new InvalidLoginCredentialsException());
	}

	@Override
	public Customer findByCustomernameOrEmail(String customername, String email) {
		Optional<Customer> customer = customerRepository.findByUsernameOrEmail(customername, email);
		if (customer.isPresent())
			return customer.get();
		return null;
	}

	@Override
	public Customer findByCustomerId(long customerId) {
		return customerRepository.findByCustomerId(customerId).orElseThrow(() -> new UserNotFoundException());
	}

}
