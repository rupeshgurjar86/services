package com.mykart.customer.response;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@ToString
@NoArgsConstructor
public class CustomerResponse {
	private String username;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String address;
	
	@Setter
	@Getter
	@ToString
	@Component
	public static class CustomerLoginResponse extends CustomerResponse {
		private String accountNumber;
	}
	@Getter
	@Setter
	@ToString
	@Component
	@NoArgsConstructor
	public static class CustomerRegistrationResponse extends CustomerResponse {
	}
}
