package com.mykart.customer.mapper;

import org.springframework.stereotype.Component;

import com.mykart.customer.entity.Customer;
import com.mykart.customer.request.CustomerRequest;
import com.mykart.customer.request.CustomerRequest.CustomerLoginRequest;
import com.mykart.customer.request.CustomerRequest.CustomerRegistrationRequest;
import com.mykart.customer.request.CustomerRequest.CustomerUpdateRequest;
import com.mykart.customer.response.CustomerResponse;
import com.mykart.customer.response.CustomerResponse.CustomerLoginResponse;
import com.mykart.customer.response.CustomerResponse.CustomerRegistrationResponse;
@Component
public class CustomerMapper {
	public Customer mapCustomerRegistrationRequest(CustomerRegistrationRequest request) {
		Customer customer = commonCustomerRequestFieldMapper(request);
		return customer;

	}
	public Customer mapCustomerUpdateRequest(CustomerUpdateRequest request) {
		Customer customer = commonCustomerRequestFieldMapper(request);
		customer.setAccountNumber(request.getAccountNumber());
		return customer;
	}

	public CustomerRegistrationResponse mapRegistrationResponse(Customer customer) {
		CustomerResponse response = new CustomerRegistrationResponse();
		CustomerResponse customerResponse = commonCustomerResponseFieldMapper(customer, response);
		CustomerRegistrationResponse finalResponse = (CustomerRegistrationResponse) customerResponse;
		return finalResponse;
	}

	public CustomerLoginResponse mapLoginResponse(Customer customer) {
		CustomerResponse response = new CustomerLoginResponse();
		CustomerResponse customerResponse = commonCustomerResponseFieldMapper(customer, response);
		CustomerLoginResponse finalResponse = (CustomerLoginResponse) customerResponse;
		String accountNumber ="";
		if(null==customer.getAccountNumber())
		   accountNumber = "Please update account number your account details";
		else
			accountNumber = customer.getAccountNumber();
		finalResponse.setAccountNumber(accountNumber);
		return finalResponse;
	}

	public Customer mapLoginRequest(CustomerLoginRequest request) {
		Customer customer = new Customer();
		customer.setUsername(request.getUsername());
		customer.setAccountNumber(request.getPassword());
		return customer;
	}

	public CustomerResponse commonCustomerResponseFieldMapper(Customer customer, CustomerResponse response) {
		response.setFirstName(customer.getFirstName());
		response.setLastName(customer.getLastName());
		response.setEmail(customer.getEmail());
		response.setPhone(customer.getPhone());
		response.setUsername(customer.getUsername());
		response.setAddress(customer.getAddress());
		return response;
	}
	
	
	
	public Customer commonCustomerRequestFieldMapper(CustomerRequest request) {
		Customer customer = new Customer();
		customer.setFirstName(request.getFirstName());
		customer.setLastName(request.getLastName());
		customer.setEmail(request.getEmail());
		customer.setPhone(request.getPhone());
		customer.setUsername(request.getUsername());
		customer.setAddress(request.getAddress());
		return customer;
	}

}
