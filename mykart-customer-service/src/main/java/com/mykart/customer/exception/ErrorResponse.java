package com.mykart.customer.exception;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class ErrorResponse {
	    private String message;
	    private Set<String> errors;


		public ErrorResponse(String message, Set<String> details) {
			super();
	        this.message = message;
	        this.errors = details;
		}
}
