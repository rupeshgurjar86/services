package com.mykart.customer.exception;

public class InvalidLoginCredentialsException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidLoginCredentialsException() {
		super("Invalid customername/password.");
	}
}
