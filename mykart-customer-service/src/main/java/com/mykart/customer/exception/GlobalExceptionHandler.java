package com.mykart.customer.exception;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", "Server Error");

		return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(UserNameAlreadyExistException.class)
	public ResponseEntity<Object> handleCustomerNameAlreadyExistException(UserNameAlreadyExistException ex,
			WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<Object>(error, HttpStatus.EXPECTATION_FAILED);
	}

	@ExceptionHandler(DataNotFoundException.class)
	public ResponseEntity<Object> handleDataNotFoundException(DataNotFoundException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<Object>(error, HttpStatus.EXPECTATION_FAILED);
	}

	@ExceptionHandler(InvalidLoginCredentialsException.class)
	public ResponseEntity<Object> handleInvalidLoginCredentialsException(InvalidLoginCredentialsException ex,
			WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<Object>(error, HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(EmailAlreadyExistException.class)
	public ResponseEntity<Object> handleEmailAlreadyExistException(EmailAlreadyExistException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<Object> handleCustomerNotFoundException(UserNotFoundException ex, WebRequest request) {
		Set<String> body = new HashSet<>();
		body.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(), body);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Set<String> body = new HashSet<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			body.add(error.getDefaultMessage());
		}

		ErrorResponse error = new ErrorResponse("Validation Failed", body);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

}
