package com.mykart.customer.exception;

public class DataNotFoundException extends RuntimeException {
	public DataNotFoundException() {
		super("No Data Found!!!");
	}
}
