package com.mykart.customer.exception;

public class EmailAlreadyExistException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmailAlreadyExistException() {
		super("Email already associated with some other account.");
	}
}
