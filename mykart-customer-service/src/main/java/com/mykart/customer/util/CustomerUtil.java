package com.mykart.customer.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mykart.customer.entity.Customer;
import com.mykart.customer.exception.UserNameAlreadyExistException;
import com.mykart.customer.repository.CustomerRepository;
import com.mykart.customer.service.CustomerService;
@Component
public class CustomerUtil {
	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerRepository cutomerRepository;
	private static Long nextCustomerId = 100000001l;
	
	public String getNextCustomerId() {
		Customer customer = null;
		if (nextCustomerId == 100000001) {
			customer = new Customer();
			customer = cutomerRepository.findFirstByOrderByCustomerIdDesc();
			if (null != customer)
				nextCustomerId = Long.valueOf(customer.getCustomerId());
		}
		String customerId = String.valueOf(++nextCustomerId);
		return customerId;
	}

	public boolean checkCustomerExists(String customername, String email) {
		if (checkCustomernameExists(customername) || checkEmailExists(customername)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkCustomernameExists(String customername) {
		if (null == customerService.findByCustomername(customername)) {
			return true;
		} else {
			throw new UserNameAlreadyExistException();
		}
	}

	public boolean checkEmailExists(String email) {
		if (null != customerService.findByEmail(email)) {
			return true;
		}
		return false;
	}


}
