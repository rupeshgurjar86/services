package com.mykart.customer.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mykart.customer.entity.Customer;
import com.mykart.customer.request.CustomerRequest.CustomerLoginRequest;
import com.mykart.customer.request.CustomerRequest.CustomerRegistrationRequest;
import com.mykart.customer.request.CustomerRequest.CustomerUpdateRequest;
import com.mykart.customer.response.CustomerResponse;
import com.mykart.customer.response.CustomerResponse.CustomerLoginResponse;
import com.mykart.customer.response.CustomerResponse.CustomerRegistrationResponse;
import com.mykart.customer.service.CustomerService;
import com.mykart.customer.validators.CustomerRegistrationValidator;

import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/customers")
public class CustomerController {
	private static final Logger LOG = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	CustomerService customerService;

	@Autowired
	CustomerRegistrationValidator customerRegistrationValidator;

	
	@ApiIgnore
	@GetMapping("/api")
	public String test() {
		return "Customer service is up & running";
	}

	@PostMapping(value = "/signup")
	public ResponseEntity<Object> customerRegistration(@Valid @RequestBody CustomerRegistrationRequest request) {
		CustomerRegistrationResponse customerResponse = customerService.createCustomer(request);
		return new ResponseEntity<Object>(customerResponse, HttpStatus.OK);
	}

	@PostMapping(value = "/login")
	public ResponseEntity<Object> login(@Valid @RequestBody CustomerLoginRequest customer) {
		CustomerLoginResponse response = customerService.login(customer);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@ApiIgnore
	@GetMapping(value = "/{customerId}")
	public ResponseEntity<Object> findCustomer(@PathVariable("customerId") long customerId) {
		Customer customerAccountInfo = customerService.findByCustomerId(customerId);
		return new ResponseEntity<>(customerAccountInfo, HttpStatus.OK);
	}

	@PutMapping(value = "/")
	public ResponseEntity<Object> updateCustomer(@Valid @RequestBody CustomerUpdateRequest request) {
		CustomerResponse customerResponse =customerService.updateCustomer(request);
		return new ResponseEntity<Object>(customerResponse, HttpStatus.OK);
	}
	@GetMapping(value = "/username/{username}")
	public ResponseEntity<Object> findbyUsername(@PathVariable("username") String username) {
		Customer customer=customerService.findByCustomername(username);
		return new ResponseEntity<Object>(customer, HttpStatus.OK);
	}

}
