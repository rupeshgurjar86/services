package com.mykart.customer.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mykart.customer.entity.Customer;
import com.mykart.customer.exception.EmailAlreadyExistException;
import com.mykart.customer.exception.UserNameAlreadyExistException;
import com.mykart.customer.exception.UserNotFoundException;
import com.mykart.customer.service.CustomerService;

@Component
public class CustomerRegistrationValidator {
	@Autowired
	private CustomerService customerService;

	private Customer customer;
	public void  preProcessDataValidation(Customer customer,String process) {
		Customer u = customerService.findByCustomernameOrEmail(customer.getUsername(), customer.getEmail());
		if(process.equals("create")) {
			if (u != null) {
				if (customer.getEmail().equalsIgnoreCase(u.getEmail()))
					throw new EmailAlreadyExistException();
				if (customer.getUsername().equalsIgnoreCase(u.getUsername()))
					throw new UserNameAlreadyExistException();
				if (customer.getPhone().equalsIgnoreCase(u.getPhone()))
					throw new UserNameAlreadyExistException("Contanct number already used.");
			}
		}
		if(process.equals("update")||process.equals("delete")) {
			if (u == null) {
					throw new UserNotFoundException("Customer is not found or incorrect details are entered.");
			}else {
				setCustomer(u);
			}
		}
	}
	
	

	public Customer getCustomer() {
		return customer;
	}


	private void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
