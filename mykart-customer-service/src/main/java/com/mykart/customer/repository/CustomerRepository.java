package com.mykart.customer.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mykart.customer.entity.Customer;
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	Optional<Customer> findByUsername(String username);
    Customer findFirstByOrderByCustomerIdDesc();
	Optional<Customer> findByEmail(String email);
	Optional<Customer> findByUsernameAndPassword(String username,String password);
	Optional<Customer> findByUsernameOrEmail(String username,String email);
	Optional<Customer> findByCustomerId(long customerId);
	List<Customer> findAll();
	
}
