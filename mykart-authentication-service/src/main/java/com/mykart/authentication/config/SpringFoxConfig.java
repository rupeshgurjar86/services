package com.mykart.authentication.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//@Configuration
//@EnableSwagger2
public class SpringFoxConfig {
	@Bean
	public Docket api() {
		List<ResponseMessage> responseMessages = new ArrayList<>();
		ResponseMessage response500 = new ResponseMessageBuilder().code(500).message("Server Error").build();
		ResponseMessage response4041 = new ResponseMessageBuilder().code(404).message("No data found").build();
		ResponseMessage response4042 = new ResponseMessageBuilder().code(404).message("Product list is empty").build();
		ResponseMessage response4043 = new ResponseMessageBuilder().code(404).message("Product not found").build();
		responseMessages.add(response500);
		responseMessages.add(response4041);
		responseMessages.add(response4042);
		responseMessages.add(response4043);

		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.ant("/*/**")).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("MyKart Product-Service REST API", "Some custom description of API.", "API TOS",
				"Terms of service",
				new Contact("Rupesh Gurjar", "https://github.com/rupeshgurjar/spring-crud", "rupeshgurjar86@gmail.com"),
				"License of API", "API license URL", Collections.emptyList());
	}
}