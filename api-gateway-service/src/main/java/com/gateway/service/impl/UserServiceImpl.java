package com.gateway.service.impl;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.gateway.model.User;
import com.gateway.service.CustomerFeignClient;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService{
	
	@Autowired
	private CustomerFeignClient customerFeignClient;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("USer name ; "+username);
		User user = customerFeignClient.findbyUsername(username);
		System.out.println("USer name ; "+user);
		if(user==null) {
			throw new UsernameNotFoundException("Invalid username or password");
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority());
	}

	public User findOne(String username) {
		return customerFeignClient.findbyUsername(username);
	}
	private Collection<? extends GrantedAuthority> getAuthority() {
		
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}
}
