package com.gateway.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.gateway.model.User;

@FeignClient(value = "customer-service", url = "http://localhost:9082/customers")
public interface CustomerFeignClient {
	@GetMapping(value = "/username/{username}")
	public User findbyUsername(@PathVariable("username") String username);
}
